section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
	mov rax, 60
	xor rdi, rdi 
	syscall
	ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
	.loop:	
		cmp byte [rdi+rax], 0	
		je .return				
		inc rax					
		jmp .loop
	.return:
		ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	call string_length ; str_length -> rax
	mov rdx, rax ; str_len(rax) -> rdx (rdx cont. string length on syscall)
	mov rsi, rdi ; rdi cont. str_addr on function call, syscall expect str_addr in rsi
	mov rdi, 1 ; file_descriptor should be in rdi on syscall
	mov rax, 1 ; syscall_number should be in rax on syscall
	syscall ; syscall print
    ret
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
		mov rdi, 10
		jmp print_char

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
	push rdi ;pushing char to the top of stack
	mov rsi, rsp ; putting addr of the top of stack as syscall's string addr
	mov rdx, 1 ; str_len = 1
	mov rdi, 1 ; stdout
	mov rax, 1; number of write syscall
	syscall
	pop r9
    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
	mov rax, rdi
	mov r9, rsp ; тут хитрые манипуляции со стеком. Надо восстановить
	dec rsp
	mov byte [rsp], 0
	mov r8, 10
	.loop:
		xor rdx, rdx
		div r8
		mov rdi,rdx
		add rdi, '0'
		dec rsp
		mov byte [rsp], dil
		cmp rax, 0
		jnz .loop
	.return:
		mov rdi, rsp
		call print_string
		mov rsp, r9
		ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
	test rdi, rdi
	jns .print
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi
	.print:
		jmp print_uint
    

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
	mov rcx, -1
	.loop:
		inc rcx
		mov al, [rdi + rcx]
		mov ah, [rsi + rcx]
		cmp al ,ah
		jne .not_equal
		cmp al, 0
		jne .loop
		mov rax, 1
		ret
	.not_equal:
		xor rax, rax
		ret
   

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	xor rax, rax
	mov rdi, 0
	lea rsi, [rsp-1]
	mov rdx, 1
	syscall
	test rax, rax
	jz .stream_end
	mov  al, [rsp-1]
	ret
	.stream_end:
		xor rax, rax
		ret
	

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

;reads symbol and return:
;rax = 0, rdx = 1 if symbol is whitespace
;rax = symbol, rdx = 0 if symbol is not whitespace
check_whitespace:
	push rdi
	push rsi
	call read_char
	pop rsi
	pop rdi
	cmp al, 0x20
	je .whitespace
	cmp al, 0x9
	je .whitespace
	cmp al, 0xA
	je .whitespace
	xor rdx, rdx
	ret
	.whitespace:
		mov rax, 0
		mov rdx, 1
		ret
	
	
	
read_word:
	xor rax, rax
	xor r8, r8 ;counter
	.strip:
		call check_whitespace
		cmp rdx, 1
		je .strip
		jmp .write
	.loop:
		call check_whitespace
		.write:
			mov byte [rdi + r8], al
			test rax, rax
			jz .return
			inc r8
			cmp r8, rsi
			jg .overflow
			jmp .loop
		
	.overflow:
		xor rax, rax
		ret
	.return:
		mov rax, rdi
		mov rdx, r8
		
    ret
 

;return 10 if symbol is not digit, else return digit

isdigit:
	xor rax, rax
	sub dil, 0x30
	cmp dil, 0
	jl .non_digit
	cmp dil, 9
	jg .non_digit
	mov al, dil				
	ret
	.non_digit:
		mov rax, 10
		ret
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
	xor rcx, rcx
	mov r8, 10
	mov r10, rdi ; start string pointer
	mov rdi, 0
	.loop:
		mov byte dil, [r10 + rcx]
		push rax
		call isdigit
		cmp rax, 10
		je .return
		mov rdi, rax
		pop rax
		mul r8
		add rax, rdi
		inc rcx
		jmp .loop
	.return:
		pop rax
		mov rdx, rcx
		ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
	cmp byte [rdi], '-'
	je .negative
	call parse_uint
	ret
	.negative:
		inc rdi
		call parse_uint
		neg rax
		test rdx, rdx
		jz .return
		inc rdx
		.return:
			ret



; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер 
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	call string_length ; str_len -> rax
	cmp rax, rdx
	jge .overflow
	mov rcx, 0 ;pointer to current symbol
	.loop:
		mov r10, [rdi+rcx]		
		mov [rsi+rcx], r10
		cmp rcx, rax
		je .return
		inc rcx
		jmp .loop
		
	.overflow:
		xor rax, rax
		ret
	.return:
		ret

